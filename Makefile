IMAGE = registry.gitlab.com/oalbiez/wesnoth-server

build:
	docker build --pull -t ${IMAGE}:1.14.5 1.14.5
	docker build --pull -t ${IMAGE} 1.14.5

publish: build
	docker push ${IMAGE}:1.14.5
	docker push ${IMAGE}
